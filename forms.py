from flask_wtf import Form

from wtforms import StringField, SubmitField, IntegerField, BooleanField
from wtforms.validators import Required


class orderForm(Form):
	laptop = BooleanField('Add Items to cart')
	menshirt = BooleanField('Add Items to cart')
	charger = BooleanField('Add Items to cart')
	submit = SubmitField('Order')


class CustomerName(Form):
	name = StringField('Customer Name ', validators=[Required()])
	submit = SubmitField('Order')
