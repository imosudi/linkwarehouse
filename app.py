from flask import Flask, render_template, redirect, url_for, request, session, flash, send_from_directory, send_file
from flask_script import Manager
from flask_moment import Moment

from flask_bootstrap import Bootstrap
from flask_wtf import Form
from wtforms import StringField, SubmitField, IntegerField
from flask.ext.sendmail import Message
from wtforms.validators import Required, Email
from flask.ext.mail import Mail

from forms import *

mail = Mail()

app = Flask(__name__)
app.config['SECRET_KEY'] = 'hard to guess string'
app.config['ORDER_MAIL_SUBJECT_PREFIX'] = '[WareHouse Connect]'
app.config['ORDER_MAIL_SENDER'] = 'Product Order Admin <email@company.com>'


bootstrap = Bootstrap(app)
mail.init_app(app)


manager = Manager(app)
moment = Moment(app)

@app.route('/', methods = ['GET', 'POST'])
def index():
	form = orderForm()
	formCustomer = CustomerName()
	item = None
	if form.validate_on_submit():
		item = form.Add_Items_to_Cart.data
	return render_template('index.html', form=form, formCustomer=formCustomer, item=item)

@app.errorhandler(500)
def internal_server_error(e):
	return render_template('500.html'), 500


@app.errorhandler(404)
def page_not_found(e):
	return render_template('404.html'), 404


if __name__ == "__main__":
    app.run()